//
//  NORScannedPeripheral.swift
//  nRF Toolbox
//
//  Created by Mostafa Berg on 28/04/16.
//  Copyright © 2016 Nordic Semiconductor. All rights reserved.
//

import UIKit
import CoreBluetooth

enum FirmwareType {
    case bootloader, blinkyDemo, hltApplication, unknown
}

enum FotaStateMachine {
    case stateTbd
    case appIsRunning
    case fotaRequestSent
    case bootloaderIsRunning
    case fotaInProcess
}

@objc class NORScannedPeripheral: NSObject {
    
    var peripheral  : CBPeripheral
    var RSSI        : Int32
    var isConnected : Bool
    var isDisconnectedFromPhone : Bool
    var bleAddr     : [UInt8]
    var firmwareType : FirmwareType
    var firmwareRev : FirmwareRev
    var fotaState = FotaStateMachine.stateTbd
    var watchDog = 0
    var leftRight = -1               //0: left, 1: right, -1: unknown
    
    init(withPeripheral aPeripheral: CBPeripheral, andRSSI anRSSI:Int32 = 0, andIsConnected aConnectionStatus: Bool) {
        peripheral = aPeripheral
        RSSI = anRSSI
        isConnected = aConnectionStatus
        isDisconnectedFromPhone = false
        bleAddr = []
        firmwareType = .unknown
        firmwareRev = FirmwareRev(mainRev: 0, subRev: 0, minorRev: 0)
        leftRight = -1
    }
    
    func name()->String{
        let peripheralName = peripheral.name
        if peripheral.name == nil {
            return "No name"
        } else {
            return peripheralName!
        }
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let otherPeripheral = object as? NORScannedPeripheral {
            return peripheral == otherPeripheral.peripheral
        }
        return false
    }
}
