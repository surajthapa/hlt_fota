//
//  ViewController.swift
//  BLE Test
//
//  Created by Paul Wang on 2018-08-09.
//  Copyright © 2018 ict. All rights reserved.
//

import UIKit
import CoreBluetooth


class ViewController: UIViewController, CBCentralManagerDelegate, CBPeripheralDelegate, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableViewDeviceList: UITableView!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var lblProgress: UILabel!
  
    let newFirmwareRev = FirmwareRev(mainRev: 0, subRev: 0, minorRev: 15)
    let firmwareFilename = "MFi_PLC_HLT_0_0_F";
    
    let dfuFotaServiceUUID                  = CBUUID(string: "6E400001-B5A3-F393-10A9-E50E24DCCA9E")
    let dfuCharStartAddrUUID                = CBUUID(string: "FFFFFF00-0000-0000-0000-000001012222")
    let dfuCharTransferControlUUID          = CBUUID(string: "FFFFFF00-0000-0000-0000-000001022222")
    let dfuCharDataUUID                     = CBUUID(string: "FFFFFF00-0000-0000-0000-000002022222")
    let dfuBlinkyServiceUUID                = CBUUID(string: "916DC396-5B9B-4577-810D-1B98C60784CA")
    let dfuCharTriggerUUID                  = CBUUID(string: "66A27254-6C20-4FBE-AEC7-6DD41AAf7EE5")
    let dfuCharRevBleAddrFromAppUUID        = CBUUID(string: "E093F3B5-00A3-A9E5-9ECA-40026E0EDC24")
    let dfuCharRevBleAddrFromBootloaderUUID = CBUUID(string: "6E400001-B5A3-F393-10A9-E50E0001CA9E")
    let haServiceUUID                       = CBUUID(string: "7D74F4BD-C74A-4431-862C-CCE884371592")
    let haCharLeftRightUUID                 = CBUUID(string: "8D17AC2F-1D54-4742-A49A-EF4B20784EB3")
    
    var charStartAddr                   : CBCharacteristic?
    var charTransferControl             : CBCharacteristic?
    var charData                        : CBCharacteristic?
    var charTrigger                     : CBCharacteristic?
    var charRevBleAddrFromApp           : CBCharacteristic?
    var charRevBleAddrFromBootloader    : CBCharacteristic?
    var charHaLeftRight                 : CBCharacteristic?

    var bluetoothManager : CBCentralManager?
    var delegate         : NORScannerDelegate?
    var peripherals      : [NORScannedPeripheral] = []
    var selectedPeripheral: CBPeripheral?
    var selectedApp: CBPeripheral?
    var scanTimer        : Timer?
    var dfuTimer: Timer?
    var requiredServices : [CBUUID]? {
        return nil
    }
    
    
    var numPackets = 0
    var packetIndex = 0
    var binaries = Data()
    var bleAddrConfirmedFota : [UInt8]?
    var startToFotaByTappingBootloader = false
    var fotaInprocess = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        progressBar.setProgress(0.0, animated: true)
        lblProgress.text = NSString(format: "%i%%", Int(progressBar.progress * 100)) as String
        let centralQueue = DispatchQueue(label: "no.nordicsemi.nRFToolBox", attributes: [])
        bluetoothManager = CBCentralManager(delegate: self, queue: centralQueue)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return peripherals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let aCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let scannedPeripheral = peripherals[indexPath.row]
        
        var leftRightStr = ""
        if scannedPeripheral.leftRight == 0 {
            leftRightStr = "Left"
        } else if scannedPeripheral.leftRight == 1 {
            leftRightStr = "Right"
        }
        
        aCell.textLabel?.text = scannedPeripheral.name() + " " + leftRightStr + " " + scannedPeripheral.firmwareRev.getString()
        if !scannedPeripheral.isConnected {
            //only display unconnected unit in the list
            aCell.imageView!.image = self.getRSSIImage(RSSI: scannedPeripheral.RSSI)
        }
        
        return aCell
    }
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //bluetoothManager?.stopScan()
        //        let success = scanForHearingAids(false)
        //        if !success {
        //            print("Bluetooth is powered off!")
        //        }
        if !fotaInprocess {
            let device = peripherals[indexPath.row]
            
            //display the revision and warning for firmware upgrading
//            if newFirmwareRev > device.firmwareRev || device.firmwareType == .bootloader {
                let alert = UIAlertController(title: "Do you want to upgrade the firmware?", message: "New firmware version: \(newFirmwareRev.getString())", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action) in
                    alert.dismiss(animated: true, completion: nil)
                    
                    self.bleAddrConfirmedFota = device.bleAddr
                    
                    self.selectedPeripheral = self.peripherals[indexPath.row].peripheral
                    self.bluetoothManager?.connect(self.selectedPeripheral!, options: nil)
                    
                }))
                
                alert.addAction(UIAlertAction(title: "NO", style: .default, handler: { (action) in
                    alert.dismiss(animated: true, completion: nil)
                    
                    self.selectedPeripheral = nil
                    self.bleAddrConfirmedFota = []               //no device is confirmed to be FOTAed, set it to empty
                }))
                
                self.present(alert, animated: true, completion: nil)
//            } else {
//                let alert = UIAlertController(title: "You already have the latest firmware", message: "Firmware installed: \(device.firmwareRev.getString())", preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//            }
            
        }

    }
    

    
    func startFota(peripheral : CBPeripheral) {
        selectedPeripheral = peripheral
        
        let filePath = Bundle.main.path(forResource: firmwareFilename, ofType: "hex")
        print(filePath!)
        let urlToHexFile = URL(fileURLWithPath: filePath!)
        let hexData = try! Data(contentsOf: urlToHexFile)
        
        binaries = IntelHex2BinConverter.convert(hexData)
        print("firmware size: " + String(binaries.count))
        
        var checksum = 0
        for i in 0..<binaries.count {
            checksum = checksum + Int(binaries[i])
        }
        print("firmware checksum: " + String(checksum))

        fotaInprocess = true
        
        // send start address
        var startAddr = Data()
        startAddr.append(contentsOf: [0x00, 0x00, 0x10, 0x00])
        
        selectedPeripheral?.writeValue(startAddr, for: charStartAddr!, type: .withoutResponse)
        print("write start address")
        
        Thread.sleep(forTimeInterval: 0.1)
        
        // send control
        var controlData = Data()
        controlData.append(contentsOf: [0x01, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])
        controlData[5] = UInt8((binaries.count >> 24) & 0xff)
        controlData[6] = UInt8((binaries.count >> 16) & 0xff)
        controlData[7] = UInt8((binaries.count >> 8) & 0xff)
        controlData[8] = UInt8(binaries.count & 0xff)
        controlData[9] = 0x00
        controlData[10] = UInt8((checksum >> 24) & 0xff)
        controlData[11] = UInt8((checksum >> 16) & 0xff)
        controlData[12] = UInt8((checksum >> 8) & 0xff)
        controlData[13] = UInt8(checksum & 0xff)
        
        selectedPeripheral?.writeValue(controlData, for: charTransferControl!, type: .withoutResponse)
        print("write control")
        Thread.sleep(forTimeInterval: 0.1)
        
        DispatchQueue.main.async {
            self.dfuTimer = Timer.scheduledTimer(timeInterval: 0.005, target: self, selector: #selector(self.onePacketTimer), userInfo: nil, repeats: true)
        }
        
        numPackets = (binaries.count + 18) / 19
        packetIndex = 0
        DispatchQueue.main.async {
            self.progressBar.setProgress(0.0, animated: true)
        }
    }
    
    func getRSSIImage(RSSI anRSSIValue: Int32) -> UIImage {
        var image: UIImage

        if (anRSSIValue < -90) {
            image = UIImage(named: "Signal_0")!
        } else if (anRSSIValue < -70) {
            image = UIImage(named: "Signal_1")!
        } else if (anRSSIValue < -50) {
            image = UIImage(named: "Signal_2")!
        } else {
            image = UIImage(named: "Signal_3")!
        }
        
        return image
    }
    
    @objc func onePacketTimer() {
        var packet = Data(count: 20)
        var start  = 0
        var end = 0
        packet[0] = UInt8(packetIndex & 0xff)
        start = packetIndex * 19
        end = packetIndex * 19 + 18
        if end > binaries.count - 1 {
            end = binaries.count - 1
        }
        packet.replaceSubrange(Range(1...((end - start) + 1)), with: binaries.subdata(in: Range(start...end)))
        
        selectedPeripheral?.writeValue(packet, for: charData!, type: .withoutResponse)
   
        let progress = Float(packetIndex) / Float(numPackets - 1)
        if (progress > progressBar.progress + 0.01) {
            progressBar.setProgress(progress, animated: true)
            lblProgress.text = NSString(format: "%i%%", Int(progressBar.progress * 98)) as String       //use 98 to make it never get to 100% until the last step
        }

        packetIndex += 1
        
        if (packetIndex == numPackets) {
            dfuTimer?.invalidate()
            
            progressBar.setProgress(1.0, animated: true)
            lblProgress.text = NSString(format: "%i%%", Int(progressBar.progress * 100)) as String
            Thread.sleep(forTimeInterval: 0.5)
            
            selectedPeripheral?.writeValue(Data([0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]), for: charTransferControl!, type: .withoutResponse)
            print("write control all 0's")
            
            Thread.sleep(forTimeInterval: 0.5)
            
            selectedPeripheral?.writeValue(Data([0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff]), for: charTransferControl!, type: .withoutResponse)
            print("write control all 1's")
            
            fotaInprocess = false
            
            selectedPeripheral = nil
            
            let alert = UIAlertController(title: "Info", message: "Firmware upgrade completed succussfully. Please open and close the battery door.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            DispatchQueue.main.async {
                self.present(alert, animated: true, completion: nil)
            }
            
            
//            let success = scanForHearingAids(true)
//            if !success {
//                print("Bluetooth is powered off!")
//            }
//
        }
    }
    
    @objc func timerFire() {
        if peripherals.count > 0 {
            tableViewDeviceList.reloadData()

            for (index, peri) in peripherals.enumerated() {
                peri.watchDog += 1
                
                if peri.watchDog > 5 {
                    peripherals.remove(at: index)
                    tableViewDeviceList.reloadData()
                }
            }
        }
    }
    
    func getConnectedPeripherals() -> [CBPeripheral] {
        guard let bluetoothManager = bluetoothManager else {
            return []
        }
        
        var retreivedPeripherals : [CBPeripheral]
        retreivedPeripherals     = bluetoothManager.retrieveConnectedPeripherals(withServices: [dfuFotaServiceUUID])

        return retreivedPeripherals
    }

    /**
     * Starts scanning for peripherals with rscServiceUUID.
     * - parameter enable: If YES, this method will enable scanning for bridge devices, if NO it will stop scanning
     * - returns: true if success, false if Bluetooth Manager is not in CBCentralManagerStatePoweredOn state.
     */
    func scanForHearingAids(_ enable:Bool) -> Bool {
        guard bluetoothManager?.state == .poweredOn else {
            return false
        }
        
        DispatchQueue.main.async {
            if enable == true {
                let options: NSDictionary = NSDictionary(objects: [NSNumber(value: true as Bool)], forKeys: [CBCentralManagerScanOptionAllowDuplicatesKey as NSCopying])
                let filter = [self.dfuFotaServiceUUID, self.haServiceUUID, self.dfuBlinkyServiceUUID]
                self.bluetoothManager?.scanForPeripherals(withServices: filter, options: options as? [String : AnyObject])
                self.scanTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.timerFire), userInfo: nil, repeats: true)
            } else {
                self.scanTimer?.invalidate()
                self.scanTimer = nil
                self.bluetoothManager?.stopScan()
            }
        }
        
        return true
    }

    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        // Scanner uses other queue to send events. We must edit UI in the main queue
        
        DispatchQueue.main.async(execute: {
            var hearingAid = NORScannedPeripheral(withPeripheral: peripheral, andRSSI: RSSI.int32Value, andIsConnected: false)
//            print("peripheral identifier: ")
//            print(peripheral.identifier)
            if ((self.peripherals.contains(hearingAid)) == false) {
                print("BLE device discovered")
                
                self.peripherals.append(hearingAid)
                
                let advertisedUUIDString = advertisementData[CBAdvertisementDataServiceUUIDsKey] as! [CBUUID]
//                let manufacturerData = advertisementData[CBAdvertisementDataManufacturerDataKey]
                
//                print("--------------------")
//                print(manufacturerData)
                
                
                if advertisedUUIDString[0].isEqual(self.dfuFotaServiceUUID) {
                    print("FOTA Bootloader is advertising ...")
                    hearingAid.firmwareType = .bootloader
                    hearingAid.fotaState = .bootloaderIsRunning
                    self.bluetoothManager?.connect(peripheral, options: nil)
                } else if advertisedUUIDString[0].isEqual(self.dfuBlinkyServiceUUID) {
                    print("Paul's blinky demo is advertising ...")
                    hearingAid.firmwareType = .blinkyDemo
                    hearingAid.fotaState = .appIsRunning
                    self.bluetoothManager?.connect(peripheral, options: nil)
                } else if advertisedUUIDString[0].isEqual(self.haServiceUUID) {
                    print("HLT App is advertising ...")
                    hearingAid.firmwareType = .hltApplication
                    hearingAid.fotaState = .appIsRunning
                    self.bluetoothManager?.connect(peripheral, options: nil)
                }
            } else {
                hearingAid = self.peripherals[self.peripherals.index(of: hearingAid)!]
                hearingAid.RSSI = RSSI.int32Value
                hearingAid.watchDog = 0
            }
        })
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        guard central.state == .poweredOn else {
            print("Bluetooth is porewed off")
            return
        }
        
        print("Bluetooth is now scanning ...")
        
//        let connectedPeripherals = self.getConnectedPeripherals()
//       var newScannedPeripherals: [NORScannedPeripheral] = []
//        connectedPeripherals.forEach { (connectedPeripheral: CBPeripheral) in
//            let connected = connectedPeripheral.state == .connected
//            let scannedPeripheral = NORScannedPeripheral(withPeripheral: connectedPeripheral, andIsConnected: connected )
//            newScannedPeripherals.append(scannedPeripheral)
//        }
//        peripherals = newScannedPeripherals
        let success = self.scanForHearingAids(true)
        if !success {
            print("Bluetooth is powered off!")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        //there is nothing to do here otherwise start discovering services right away
        
        print("connected to \(peripheral.name ?? "Unknow Name")")
        
        let services = requiredServices
        
        print("Discovering servicess ...")
        peripheral.delegate = self
        peripheral.discoverServices(services)

    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        //again not too much to do here otherthan start discovering the characteristics
        
        guard error == nil else {
            print("Services discovery failed!")
            return
        }
        
        print("Services discovered")
        
        for service in peripheral.services! {
            print(service.uuid.uuidString)
            
            //because we have put filter for the services, so don't need check before we discover the characteristic
            print("Discovering all characteristics in this service")
            peripheral.discoverCharacteristics(nil, for: service)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if error != nil {
            print("Characteristics discovery failed")
        } else {
            print("characteristics discovered")

//            var isInBootloader = false
//            var isRevCharFound = false
//
            for characteristic in service.characteristics! {
                if (characteristic.uuid.isEqual(dfuCharRevBleAddrFromBootloaderUUID)) {
                    charRevBleAddrFromBootloader = characteristic
                    peripheral.readValue(for: charRevBleAddrFromBootloader!)
//                    isInBootloader = true
                } else if (characteristic.uuid.isEqual(dfuCharStartAddrUUID)) {
                    charStartAddr = characteristic
//                    isInBootloader = true
                } else if (characteristic.uuid.isEqual(dfuCharTransferControlUUID)) {
                    charTransferControl = characteristic
//                    isInBootloader = true
                } else if (characteristic.uuid.isEqual(dfuCharDataUUID)) {
                    charData = characteristic
//                    isInBootloader = true
                } else if (characteristic.uuid.isEqual(dfuCharTriggerUUID)) {
                    charTrigger = characteristic
//                    isInBootloader = false
                } else if (characteristic.uuid.isEqual(dfuCharRevBleAddrFromAppUUID)) {
                    charRevBleAddrFromApp = characteristic
                    peripheral.readValue(for: charRevBleAddrFromApp!)
//                    isInBootloader = false
//                    isRevCharFound = true
                } else if (characteristic.uuid.isEqual(haCharLeftRightUUID)) {
                    charHaLeftRight = characteristic
                    peripheral.readValue(for: characteristic)
                }
            }
            
//            if isInBootloader {
//                peripheral.readValue(for: charRevBleAddrFromBootloader!)
//            }
//            
//            if isRevCharFound {
//                peripheral.readValue(for: charRevBleAddrFromApp!)
//            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        if error != nil {
            print("update value failed!")
        } else {
            print("value updated")
            
            var realData: [UInt8] = []
            realData = Array(characteristic.value!)
            print(realData)
            
            if characteristic == charRevBleAddrFromApp {
                let appFirmwareRev = FirmwareRev(mainRev: Int(realData[0]), subRev: Int(realData[1]), minorRev: Int(realData[2]))

                print("found firmware revision and ble address from application")
                print(appFirmwareRev)
                let bleAddr = Array(realData.suffix(from: 3))
                print("ble address:")
                print(bleAddr)
                
                for peri in peripherals {
                    if (peri.peripheral == peripheral) {
                        peri.bleAddr = bleAddr
                        peri.firmwareRev = appFirmwareRev
                        
                        DispatchQueue.main.async {
                            self.tableViewDeviceList.reloadData()
                        }
                        break
                    }
                }
                
                if (bleAddr == bleAddrConfirmedFota) {
                    print("user confirmed to trigger bootloader")

                    peripheral.writeValue(Data([0x55, 0xaa]), for: self.charTrigger!, type: .withoutResponse)
                    
                    //after send out the trigger bootloader command, start scan again to see if bootload service is up
             //       peripherals.removeAll()
                    
//                    DispatchQueue.main.async {
//                        self.tableViewDeviceList.reloadData()
//                    }
                    
//                    let success = self.scanForHearingAids(true)
//                    if !success {
//                        print("Bluetooth is powered off!")
//                    }
                } else {
                    print("the device with application was just got scan, no user actions yet")
                    
                    for peri in self.peripherals {
                        if (peri.peripheral == peripheral) {
                            self.bluetoothManager?.cancelPeripheralConnection(peripheral)
                            peri.isDisconnectedFromPhone = true
                            break
                        }
                    }
                }
            } else if characteristic == charRevBleAddrFromBootloader {
                let bootloaderFirmwareRev = FirmwareRev(mainRev: Int(realData[0]), subRev: Int(realData[1]), minorRev: Int(realData[2]))

                print("found firmware revision and ble address from bootloader")
                print(bootloaderFirmwareRev)
                let bleAddr = Array(realData.suffix(from: 3))
                print("ble address:")
                print(bleAddr)
                for peri in self.peripherals {
                    if (peri.peripheral == peripheral) {
                        peri.bleAddr = bleAddr
                        peri.firmwareRev = bootloaderFirmwareRev
                        
                        DispatchQueue.main.async {
                            self.tableViewDeviceList.reloadData()
                        }

                        break
                    }
                }

                /*
                if (bleAddr == bleAddrConfirmedFota) {
                    print("this bootloader on this device was just triggered by this phone, download firmware direclty")
                    bleAddrConfirmedFota = []               //set it to nil to avoid next time automatically upgrade again

                    startFota(peripheral: peripheral)
                } else {
                    print("the device was in bootloader mode, waiting for customer to confirm to download firmware")
                    
                    for peri in self.peripherals {
                        if (peri.peripheral == peripheral) {
                            self.bluetoothManager?.cancelPeripheralConnection(peripheral)
                            peri.isDisconnectedFromPhone = true
                            break
                        }
                    }
                }
                */

                if !fotaInprocess {
                    startFota(peripheral: peripheral)
                }
                
                //in some cases after hearing aid retart into bootloader the bleaddr isn't same as the one has been confirmed
                //so we decided to upgrade it without use tapping it again
                
            } else if characteristic == charHaLeftRight {
                var realData: [UInt8] = []
                realData = Array(characteristic.value!)
                print("left right value:")
                print(realData)
                
                for peri in self.peripherals {
                    if (peri.peripheral == peripheral) {
                        peri.leftRight = Int(realData[0])
                        
                        DispatchQueue.main.async {
                            self.tableViewDeviceList.reloadData()
                        }
                        
                        break
                    }
                }

            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        if error != nil {
            print("write value failed")
            print(error!)
        } else {
            print("write value response received")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("device disconnected!")

        if (fotaInprocess) {
            if (peripheral == selectedPeripheral) {
                //FOTA interrupted, stop the timer and alert
                dfuTimer?.invalidate()
                fotaInprocess = false
                
                let alert = UIAlertController(title: "Warning", message: "Firmware upgrading has been interrupted.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                DispatchQueue.main.async {
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        
        //peripherals.removeAll()
        for (idx, peri) in peripherals.enumerated().reversed() {
            if (peri.peripheral == peripheral) {
                if (!peri.isDisconnectedFromPhone) {
                    //if it is disconnected after phone reads the firmware revision, don't remomve it from the list
                    //only when the peripheral disconnect, then remove it from the list
                    peripherals.remove(at: idx)
                } else {
                    peri.isDisconnectedFromPhone = false
                }
            }
        }
        
        DispatchQueue.main.async {
            self.tableViewDeviceList.reloadData()
        }
    }
}

