//
//  AppDelegate.swift
//  BLE Test
//
//  Created by Paul Wang on 2018-08-09.
//  Copyright © 2018 ict. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Thread.sleep(forTimeInterval: 1.0)
        // Override point for customization after application launch.
 
        
        
        do {
            let update = try self.isUpdateAvailable()
            DispatchQueue.main.async {
                // show alert
                if update {
                    print("Update available")
                    
                    let alert = UIAlertController(title: "New version available", message: "You need the latest version to upgrade your hearing aids.", preferredStyle: .alert)
 
                    alert.addAction(UIAlertAction(title: "Go to App Store", style: .default, handler: { (action) in
                        alert.dismiss(animated: true, completion: nil)
    
                        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                        
                        let appStoreLink = "https://itunes.apple.com/us/app/lucid-hearing/id1435854916?mt=8"
                        
                        /* First create a URL, then check whether there is an installed app that can
                         open it on the device. */
                        if let url = URL(string: appStoreLink), UIApplication.shared.canOpenURL(url) {
                            // Attempt to open the URL.
                            UIApplication.shared.open(url, options: [:], completionHandler: {(success: Bool) in
                                if success {
                                    print("Launching \(url) was successful")
                                    print("exit the application")
                                    exit(0)
                                }})
                        }
                    }))
                    
                    self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                } else {
                    print("There is no newer version from the App Store")
                }
            }
        } catch {
            print("something went wrong")
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    enum VersionError: Error {
        case invalidResponse, invalidBundleInfo
    }
    
    func isUpdateAvailable() throws -> Bool {
        guard let info = Bundle.main.infoDictionary,
            let currentVersion = info["CFBundleShortVersionString"] as? String,
            let identifier = info["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
                throw VersionError.invalidBundleInfo
        }
        let data = try Data(contentsOf: url)
        guard let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any] else {
            throw VersionError.invalidResponse
        }
        if let result = (json["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
            let currentVersionNumbers = getRevisionNumbers(currentVersion)
            let appStoreVersionNumbers = getRevisionNumbers(version)

            if appStoreVersionNumbers.mainVersion > currentVersionNumbers.mainVersion {
                return true
            } else if appStoreVersionNumbers.mainVersion < currentVersionNumbers.mainVersion {
                return false
            } else {
                if appStoreVersionNumbers.subVersion > currentVersionNumbers.subVersion {
                    return true
                } else {
                    return false
                }
            }
        }
        throw VersionError.invalidResponse
    }
    
    func getRevisionNumbers(_ versionString: String) -> (mainVersion: Int, subVersion: Int) {
        //remove "V" at beginning
        var str = versionString
        if versionString.starts(with: "V") {
            str = String(versionString.dropFirst())
        }
        
        let dotIndex = str.firstIndex(of: ".")
        let afterDot = str.index(dotIndex!, offsetBy: 1)
        
        let mainVersionStr = String(str.prefix(upTo: dotIndex!))
        let subVersionStr = String(str.suffix(from: afterDot))
        
        return (Int(mainVersionStr)!, Int(subVersionStr)!)
    }
}

