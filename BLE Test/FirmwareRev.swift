//
//  FirmwareRev.swift
//  BLE Test
//
//  Created by ZHIJIAN WANG on 2018-10-09.
//  Copyright © 2018 ict. All rights reserved.
//

import Foundation

struct FirmwareRev {
    var mainRev: Int
    var subRev: Int
    var minorRev: Int
    
    func getString() -> String {
        if mainRev == 0 && subRev == 0 && minorRev == 0 {
            return ""
        } else {
            return String("V\(mainRev).\(subRev).\(minorRev)")
        }
    }
    
    static func > (left: FirmwareRev, right: FirmwareRev) -> Bool {
        if left.mainRev > right.mainRev {
            return true
        } else if left.mainRev < right.mainRev {
            return false
        } else {
            if left.subRev > right.subRev {
                return true
            } else if left.subRev < right.subRev {
                return false
            } else {
                if left.minorRev > right.minorRev {
                    return true
                } else {
                    return false
                }
            }
        }
    }
    
    static func < (left: FirmwareRev, right: FirmwareRev) -> Bool {
        if left.mainRev < right.mainRev {
            return true
        } else if left.mainRev > right.mainRev {
            return false
        } else {
            if left.subRev < left.subRev {
                return true
            } else if left.subRev > left.subRev {
                return false
            } else {
                if left.minorRev < left.minorRev {
                    return true
                } else {
                    return false
                }
            }
        }
    }
    
    static func == (left: FirmwareRev, right: FirmwareRev) -> Bool {
        if left.mainRev == right.mainRev && left.subRev == right.subRev && left.minorRev == right.minorRev {
            return true
        } else {
            return false
        }
    }
}
